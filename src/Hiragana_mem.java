// Clavier
import java.awt.*;
import java.util.Scanner;

import java.util.Random;

public class Hiragana_mem {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    // Scanner pour obtenir des infos de la part de l'utilisateur
    Scanner scanner = new Scanner(System.in);
    String read;

    String vowel = "aiueo";
    String consonant = "xkstn";


    private void start(){
        System.out.println(ANSI_BLUE + "######################################" + ANSI_RESET);
        System.out.println(ANSI_BLUE + "#" + ANSI_YELLOW + " HIRAGANA MEMORY GAME (K to N)" + ANSI_CYAN + " v1.0 " + ANSI_BLUE + "#" + ANSI_RESET);
        System.out.println(ANSI_BLUE + "######################################\n\n" + ANSI_RESET);

        System.out.print(ANSI_BLUE + "Start ? y/n : " + ANSI_RESET);
    }

    private String get_syllable(){
        int var1 = (int)(Math.random() * (vowel.length() - 1) );
        int var2 = (int)(Math.random() * (consonant.length() - 1) );

        if(consonant.charAt(var2) == 'x') return String.valueOf(vowel.charAt(var1));

        String s = String.valueOf(consonant.charAt(var2))+ String.valueOf(vowel.charAt(var1));
        if(s.equals("si")) return "shi";
        else if(s.equals("ti")) return "chi";
        else if(s.equals("tu")) return "tsu";
        return String.valueOf(consonant.charAt(var2))+ String.valueOf(vowel.charAt(var1));
    }

    public static void main(String[] args) {

        Hiragana_mem app = new Hiragana_mem();
        app.start();

        app.read = app.scanner.nextLine();

        while(!app.read.equals("y") && !app.read.equals("n"))
            app.read = app.scanner.nextLine();

        if (app.read.equals("y")){
            System.out.println(ANSI_BLUE + "Write 'end' whenever you want to stop ! \n\n" + ANSI_RESET);


            while(!app.read.equals("end")) {
                System.out.println(ANSI_RED + app.get_syllable() + "\n" + ANSI_PURPLE);
                app.read = app.scanner.nextLine();
            }
        }
    }

}